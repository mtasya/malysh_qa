Examples for QA <br>

[Test case](https://docs.google.com/spreadsheets/d/1yJyrV6U_sMIWhqn13Q04Dzb4fLd9g0H4d1LYC2bMbSM/edit#gid=306401338) <br>
[Test items](https://docs.google.com/spreadsheets/d/1RK0WWmu-t2oNGbc_8Gqb1VeMkmkax478QxNJFmKTido/edit#gid=0) <br>
[State Transition Diagram](https://docs.google.com/spreadsheets/d/13yDCTdc-t8aLLr3yrUkqz0T8WfNTdzEGgM95jhAYH0Y/edit#gid=0) <br>
[Decision table testing](https://docs.google.com/spreadsheets/d/1zsfaduNTgRuZEeHpWfUaASHpbYU1P0kN309EYXjRXZY/edit#gid=0) <br>